<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


app()->router->get('zoap/{key}/server', [
    'as' => 'zoap.server.wsdl',
    'uses' => 'Soap\ZoapController@server'
]);

app()->router->post('zoap/{key}/server', [
    'as' => 'zoap.server',
    'uses' => 'Soap\ZoapController@server'
]);
*/
