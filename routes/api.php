<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
app()->router->get('zoap/{key}/server', [
    'as' => 'zoap.server.wsdl',
    'uses' => '\Viewflex\Zoap\ZoapController@server'
]);

app()->router->post('zoap/{key}/server', [
    'as' => 'zoap.server',
    'uses' => '\Viewflex\Zoap\ZoapController@server'
]);
*/

Route::get('soap/gea/server', [
    'as' => 'zoap.server.wsdl',
    'uses' => 'Soap\ZoapController@server'
]);

Route::post('soap/gea/server', 'Soap\ZoapController@server')->middleware('soap');

/*
Route::post('soap/gea/server', [
    'as' => 'zoap.server',
    'uses' => 'Soap\ZoapController@server'
]);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::get('/test', 'TestController@test');
