<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letture extends Model
{
    protected $connection = 'mysql_sede';
    protected $table = 'letture';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function lavorazioni()
    {
        return $this->belongsTo('App\Lavorazione','lavorazione_id');
    }


}
