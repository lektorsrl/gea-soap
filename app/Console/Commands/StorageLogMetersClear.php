<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class StorageLogMetersClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:StorageLogMetersClear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command per pulire la cartella dei log storage. Eseguito ogni inizio del mese cancella i log meters del mese precedente tranne ultima settimana';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // script per cancellare le cartelle del mese precedente tranne ultima settimana
        $mese_scorso = Carbon::now()->subMonth()->format('m');
        $all_directories_storage_meters = preg_grep('/^([^.])/', scandir(storage_path('log_meters')));
        foreach ($all_directories_storage_meters as $name_folder){
            if(strpos($name_folder, date('Y')."-".$mese_scorso) !== false) {
                // match
                $lastday = date('t',strtotime(date('Y')."-".$mese_scorso."-01"));
                $lastday_less_seven_days = $lastday - 7;
                $explode_name_folder = explode('-',$name_folder);
                $day_name_folder = end($explode_name_folder);
                if(!($day_name_folder >= $lastday_less_seven_days && $day_name_folder <= $lastday)){
                    $path = storage_path('log_meters').'\\'.$name_folder;
                    File::deleteDirectory($path);
                }
            }
        }
    }
}
