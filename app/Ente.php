<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ente extends Model
{
    protected $table = 'ente';
    protected $primaryKey = 'ente_id';
    public $timestamps = false;
}
