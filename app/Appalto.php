<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appalto extends Model
{
    protected $table = 'appalto';
    protected $primaryKey = 'appalto_id';
    public $timestamps = false;
}
