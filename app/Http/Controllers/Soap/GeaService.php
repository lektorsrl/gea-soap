<?php


namespace App\Http\Controllers\Soap;

use App\Appalto;
use App\Caricofile;
use App\Ente;
use App\Lavorazione;
use App\LetturaDettaglio;
use App\Letture;
use Illuminate\Support\Facades\Config;
use SoapFault;
use test\Mockery\Fixtures\MethodWithParametersWithDefaultValues;
use Viewflex\Zoap\Demo\DemoProvider as Provider;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Viewflex\Zoap\Demo\Types\KeyValue;
use Viewflex\Zoap\Demo\Types\Product;

class GeaService
{
    /**
     * Returns an array of products by search criteria.
     *
     * @param \App\Http\Controllers\Soap\Meter[] $meters
     * @param string $token
     * @param string $user
     * @param string $password
     * @return \App\Http\Controllers\Soap\Meter[]
     * @throws SoapFault
     */


    public function saveMeters($meters = [], $token = '', $user = '', $password = '')
    {
        if (is_array($meters->item)) {
            $array = $meters->item;
        } else {
            $array = [$meters->item];
        }

        if (!Provider::authenticate($token, $user, $password)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

        $this->logMeters($meters);
        if ($array[0]->ditta_esterna != "") {
            $ente_id_fornitore = Ente::where('ente_nome', strtoupper($array[0]->ditta_esterna) . '-SOSTITUZIONI')->pluck('ente_id');
            if ($ente_id_fornitore[0] === null) {
                header("Status: 401");
                throw new SoapFault('SOAP-ENV:Client', 'Incorrect supplier name.');
            }
            $ente_id = $ente_id_fornitore[0];
            $appalto_id_fornitore = Appalto::where('appalto_ente_id', $ente_id)->pluck('appalto_id');
            $appalto_id = $appalto_id_fornitore[0];
        } else {
            $ente_id = Config::get('veritas_enviroments.ENTE_ID');
            $appalto_id = Config::get('veritas_enviroments.APPALTO_ID');
        }
        $lavorazione_id = $this->creaLavorazione($array[0]->descrizione_breve, $ente_id);
        $progressivo = Lavorazione::find($lavorazione_id)->lavorazione_progressivo;
        $caricofile_id = $this->creaCaricofile($progressivo, $ente_id, $appalto_id);
        $sequenza = 0;

        foreach ($array as $item) {
            $lettura = new Letture();
            $lettura->lavorazione_id = $lavorazione_id;
            $lettura->progressivo = $progressivo;
            $lettura->codice_ente = $ente_id;
            $lettura->appalto_id = $appalto_id;
            $lettura->stato = 'NUL';
            $lettura->indice_file = $caricofile_id;
            $lettura->fornitura = 'A';
            $lettura->chiave_univoca = "";
            $lettura->sequenza = ++$sequenza;
            $lettura->periodicita = "Y";
            $lettura->letmin = "0";
            $lettura->letmax = "99999999";
            $lettura->extra = $item->avviso . "*" . $item->gr_codici . "*" . $item->codifica . "*" . $item->ordine . "*" . $item->tam . "*" . $item->centro_lavoro . "*" . $item->divisione . "*" . $item->descrizione_breve . "*" . $item->ogg_all . "*" . $item->ubicazione . "*" . $item->produttore . "*" . $item->data_posa;
            $lettura->codice_utente = $item->bp;
            $lettura->utente = trim($item->cognome_bp . " " . $item->nome_bp);
            $lettura->pdr = $item->impianto;
            $lettura->indirizzo = $item->indirizzo_via;
            $lettura->civico = $item->indirizzo_civ;
            $lettura->localita = $item->indirizzo_loc;
            $lettura->comune = $item->indirizzo_loc;
            $lettura->matricola = $item->matricola;
            $lettura->ubicazione = $item->pos_batteria;
            $lettura->cifre = (int)filter_var($item->indice, FILTER_SANITIZE_NUMBER_INT);
            //$lettura->data_fine = substr($item->data_avviso,0,4)."-".substr($item->data_avviso,4,2)."-".substr($item->data_avviso,-2);
            $lettura->acc = strtoupper($item->accessibilita);
            switch (strtoupper($item->accessibilita)) {
                case "A":
                    $lettura->locazione = 11;
                    break;
                case "P":
                    $lettura->locazione = 12;
                    break;
            }
            $nota_contatore = preg_replace('/[[:^print:]]/', '', $item->nota_cont);
            $nota_contatore_c = "(" . $item->diametro . "-" . $item->produttore . ")" . $nota_contatore;
            $lettura->note = substr($nota_contatore_c,0,100);
            $lettura->latitudine = explode(";", $item->coordinate)[0];
            $lettura->longitudine = explode(";", $item->coordinate)[1];
            $lettura->codice_sblocco = '';
            $lettura->matricola_contatore_padre = $item->diametro . " - " . (int)filter_var($item->indice, FILTER_SANITIZE_NUMBER_INT);
            //contatti
            switch ($item->canale_contatto) {
                case "1":
                    $lettura->modalita_preavviso = 2;
                    $lettura->contatto_preavviso = $item->email;
                    break;
                case "2":
                    $lettura->modalita_preavviso = 3;
                    if ($item->numero_telefono <> '') {
                        $lettura->contatto_preavviso = $this->checkTelefono($item->numero_telefono);
                    } else {
                        $lettura->contatto_preavviso = $this->checkTelefono($item->numero_cellulare);
                    }
                    break;
                case "3":
                    $lettura->modalita_preavviso = 1;
                    if ($item->numero_cellulare <> '') {
                        $lettura->contatto_preavviso = $this->checkTelefono($item->numero_cellulare);
                    } else {
                        $lettura->contatto_preavviso = $this->checkTelefono($item->numero_telefono);
                    }
                    break;
            }
            $lettura->save();

            $letturaDettaglio = new LetturaDettaglio();
            $letturaDettaglio->lavorazione_id = $lettura->lavorazione_id;
            $letturaDettaglio->progressivo = $lettura->progressivo;
            $letturaDettaglio->id_lettura = $lettura->id;
            $letturaDettaglio->codice_ente = $lettura->codice_ente;
            $letturaDettaglio->col1 = trim(substr($item->avviso, 0, 50));
            $letturaDettaglio->col2 = trim(substr($item->descrizione_breve, 0, 50));
            $letturaDettaglio->col3 = $item->data_avviso;
            $letturaDettaglio->col10 = $item->descrizione;
            $letturaDettaglio->save();
        }
        return $meters;
    }

    public function checkTelefono($telefono)
    {
        $telefono = trim($telefono);
        $telefono = str_replace("+39", "0039", $telefono);
        $telefono = str_replace(" ", "", $telefono);
        $telefono = str_replace("-", "", $telefono);
        $telefono = str_replace("/", "", $telefono);
        $telefono = str_replace(".", "", $telefono);
        $telefono = str_replace(",", "", $telefono);
        $telefono = str_replace("+", "", $telefono);
        $telefono = str_replace("'", "", $telefono);
        $telefono = str_replace("nd", "", $telefono);
        if (substr($telefono, 0, 4) <> '0039') {
            $telefono = "0039" . $telefono;
        }
        return $telefono;
    }

    public function creaLavorazione($descizione, $ente_id)
    {
        $lavorazione = new Lavorazione();
        $lavorazione->lavorazione_sede_id = Config::get('veritas_enviroments.SEDE_ID');
        $lavorazione->enti = $ente_id;
        $lavorazione->ente = $ente_id;
        $lavorazione->lavorazione_importata = 1;
        $lavorazione->lavorazione_nome = $descizione;
        $lavorazione->lavorazione_data = date('Y-m-d H:i:s');
        $lavorazione->lavorazione_progressivo = Lavorazione::max('lavorazione_progressivo') + 1;
        $lavorazione->save();
        return $lavorazione->lavorazione_id;
    }

    public function creaCaricofile($progressivo, $ente_id, $appalto_id)
    {
        $caricofile = new Caricofile();
        $caricofile->caricofile_progressivo = $progressivo;
        $caricofile->caricofile_nome = "SostituzioniMassive_" . date('YmdHis') . ".txt";
        $caricofile->caricofile_data = date('d/m/Y');
        $caricofile->caricofile_data_in = '...';
        $caricofile->caricofile_data_out = '...';
        $caricofile->caricofile_righe_intestazione = '';
        $caricofile->caricofile_ente_id = $ente_id;
        $caricofile->caricofile_appalto_id = $appalto_id;
        $caricofile->save();
        return $caricofile->caricofile_id;
    }

    public function logMeters($meters)
    {
        $filename = date('Y_m_d_H_i_s_') . 'request.json';
        if (!file_exists(storage_path('log_meters/' . date('Y-m-d')))) {
            File::makeDirectory(storage_path('log_meters/' . date('Y-m-d')), 0711, true, true);
        }
        Storage::disk('log_meters')->put(date('Y-m-d') . "/" . $filename, json_encode($meters));
    }
}
