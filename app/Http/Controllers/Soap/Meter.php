<?php


namespace App\Http\Controllers\Soap;


class Meter
{
    /**
     * @var string
     */
    public $avviso;

    /**
     * @var string
     */
    public $gr_codici;

    /**
     * @var string
     */
    public $codifica;

    /**
     * @var string
     */
    public $ordine;

    /**
     * @var string
     */
    public $tam;

    /**
     * @var string
     */
    public $centro_lavoro;

    /**
     * @var string
     */
    public $divisione;

    /**
     * @var string
     */
    public $data_avviso;

    /**
     * @var string
     */
    public $descrizione;

    /**
     * @var string
     */
    public $descrizione_breve;

    /**
     * @var string
     */
    public $bp;

    /**
     * @var string
     */
    public $nome_bp;

    /**
     * @var string
     */
    public $cognome_bp;

    /**
     * @var string
     */
    public $impianto;

    /**
     * @var string
     */
    public $ogg_all;

    /**
     * @var string
     */
    public $coordinate;

    /**
     * @var string
     */
    public $ubicazione;

    /**
     * @var string
     */
    public $indirizzo_via;

    /**
     * @var string
     */
    public $indirizzo_civ;

    /**
     * @var string
     */
    public $indirizzo_loc;

    /**
     * @var string
     */
    public $matricola;

    /**
     * @var string
     */
    public $produttore;

    /**
     * @var string
     */
    public $pos_batteria;

    /**
     * @var string
     */
    public $indice;

    /**
     * @var string
     */
    public $diametro;

    /**
     * @var string
     */
    public $data_posa;

    /**
     * @var string
     */
    public $accessibilita;

    /**
     * @var string
     */
    public $nota_cont;

    /**
     * @var string
     */
    public $ditta_esterna;

    /**
     * @var string
     */
    public $canale_contatto;

    /**
     * @var string
     */
    public $numero_telefono;

    /**
     * @var string
     */
    public $numero_cellulare;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $email_pec;

    /**
     * Meter.
     *
     * @param string $avviso
     * @param string $gr_codici
     * @param string $codifica
     * @param string $ordine
     * @param string $tam
     * @param string $centro_lavoro
     * @param string $divisione
     * @param string $data_avviso
     * @param string $descrizione
     * @param string $descrizione_breve
     * @param string $bp
     * @param string $nome_bp
     * @param string $cognome_bp
     * @param string $impianto
     * @param string $ogg_all
     * @param string $coordinate
     * @param string $ubicazione
     * @param string $indirizzo_via
     * @param string $indirizzo_civ
     * @param string $indirizzo_loc
     * @param string $matricola
     * @param string $produttore
     * @param string $pos_batteria
     * @param string $indice
     * @param string $diametro
     * @param string $data_posa
     * @param string $accessibilita
     * @param string $nota_cont
     * @param string $ditta_esterna
     * @param string $canale_contatto
     * @param string $numero_telefono
     * @param string $numero_cellulare
     * @param string $email
     * @param string $email_pec
     */
    public function __construct($avviso = '', $gr_codici = '', $codifica = '', $ordine = '', $tam = '', $centro_lavoro = '', $divisione = '', $data_avviso = '', $descrizione = '', $descrizione_breve = '', $bp = '', $nome_bp = '', $cognome_bp = '',
                                $impianto = '', $ogg_all = '', $coordinate = '', $ubicazione = '', $indirizzo_via = '', $indirizzo_civ = '', $indirizzo_loc = '', $matricola = '', $produttore = '', $pos_batteria = '',
                                $indice = '', $diametro = '', $data_posa = '', $accessibilita = '', $nota_cont = '', $ditta_esterna = '', $canale_contatto = '', $numero_telefono = '', $numero_cellulare = '', $email = '', $email_pec = '')
    {
        $this->avviso = $avviso;
        $this->gr_codici = $gr_codici;
        $this->codifica = $codifica;
        $this->ordine = $ordine;
        $this->centro_lavoro = $centro_lavoro;
        $this->divisione = $divisione;
        $this->data_avviso = $data_avviso;
        $this->descrizione = $descrizione;
        $this->descrizione_breve = $descrizione_breve;
        $this->bp = $bp;
        $this->nome_bp = $nome_bp;
        $this->cognome_bp = $cognome_bp;
        $this->impianto = $impianto;
        $this->ogg_all = $ogg_all;
        $this->coordinate = $coordinate;
        $this->ubicazione = $ubicazione;
        $this->indirizzo_via = $indirizzo_via;
        $this->indirizzo_civ = $indirizzo_civ;
        $this->indirizzo_loc = $indirizzo_loc;
        $this->matricola = $matricola;
        $this->produttore = $produttore;
        $this->pos_batteria = $pos_batteria;
        $this->indice = $indice;
        $this->diametro = $diametro;
        $this->data_posa = $data_posa;
        $this->accessibilita = $accessibilita;
        $this->nota_cont = $nota_cont;
        $this->ditta_esterna = $ditta_esterna;
        $this->canale_contatto = $canale_contatto;
        $this->numero_telefono = $numero_telefono;
        $this->numero_cellulare = $numero_cellulare;
        $this->email = $email;
        $this->email_pec = $email_pec;
    }

}
