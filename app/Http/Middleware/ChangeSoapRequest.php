<?php

namespace App\Http\Middleware;

use Closure;

use http\Client\Request;
use Illuminate\Support\Facades\Log;

class ChangeSoapRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $body = str_replace('/api/soap/gea/server', 'https://geaqlyapi.gruppoveritas.it/api/soap/gea/server', $request->getContent());
        $request->initialize(
            $request->query(),
            [],
            [],
            [],
            [],
            $request->server(),
            $body
        );
        return $next($request);
    }
}
