<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caricofile extends Model
{
    protected $connection = 'mysql_sede';
    protected $table = 'caricofile';
    protected $primaryKey = 'caricofile_id';
    public $timestamps = false;
}
