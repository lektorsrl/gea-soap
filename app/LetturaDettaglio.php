<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LetturaDettaglio extends Model
{
    protected $connection = 'mysql_sede';
    protected $table = 'letture_dettaglio';
    protected $primaryKey = 'id';
    public $timestamps = false;


}
