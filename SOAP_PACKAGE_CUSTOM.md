# SOAP PACKAGE
## _Modifiche custom eseguite sul package https://github.com/laminas/laminas-soap_
#
#
#
## MODIFICHE

- `riga 199 file-app/Http/Controllers/Soap/ZoapController.php` | _public static function currentUrlRoot()_ modificato la linea **$url = url(app()->request->server()['REQUEST_URI']);** così da tirare via la **secure_url** 
- `riga 812 file-vendor/laminas/laminas-soap/src/Server.php` | _protected function setRequest($request)_ modificato la linea **$xml = $request->_toString(); in $xml = $request->getContent();** così prende il getContent() che è un file Xml settato in precedenza
- `riga 913 file-vendor/laminas/laminas-soap/src/Server.php` | _public function getSoap()_ modificato la linea **$server  = new SoapServer($this->wsdl,$options); in $server = new SoapServer(null, array('uri' => $this->wsdl));** così riesco ad inizializzare il server soap.
- `riga 992 file-vendor/laminas/laminas-soap/src/Server.php` | il comando **$soap->handle($this->request);** va a costruire l'oggetto che si trova dentro a **vendor/laminas/laminas-soap/src/Server/DocumentLiteralWrapper.php**, questa costruzione la si trova nella `riga 673 file-vendor/laminas/laminas-soap/src/Server.php` con questo comando **return $this->setObject($class);**
- `riga 106 file-vendor/laminas/laminas-soap/src/Server/DocumentLiteralWrapper.php` | la funzione **public function __call($method, $args)** chiama 4 funzioni che possono essere customizzate aggiungendo controllo all'oggetto inizializzato.
- `riga 113 file-vendor/laminas/laminas-soap/src/Server/DocumentLiteralWrapper.php` tramite questa porzione di codice **$ret = call_user_func_array([$this->object, $method], array($args[0], $args[1], $args[2] ,$args[3])); return $this->getResultMessage($method, $ret);** chiamiamo il `metodo saveMeters del file app/Http/Controllers/Soap/GeaService.php` che salverà il dato.


