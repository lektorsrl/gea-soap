# This image is only used to install project dependencies.
FROM composer:1.8.6 as composer
# Copy existing application directory contents.
COPY . /app
# Change working directory. Next commands will be launched inside here.
WORKDIR /app
# Install project dependencies.
RUN composer install --ignore-platform-reqs --optimize-autoloader --no-dev

# This is the actual image. The previous one will be destroyed.
FROM php:7.2-fpm-alpine
ARG ENV="production"
# Install dependencies and add php.ini.
RUN apk add autoconf g++ gcc  libzip-dev libxml2-dev make ttf-freefont wkhtmltopdf xvfb --no-cache \
 && docker-php-ext-install pdo_mysql mysqli zip soap \
 && cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
 && sed -i "s/^expose_php = On/expose_php = Off/" /usr/local/etc/php/php.ini \
 && sed -i "s/^memory_limit = 128M/memory_limit = 512M/" /usr/local/etc/php/php.ini \
 && sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 50M/" /usr/local/etc/php/php.ini \
 && sed -i "s/post_max_size = 8M/post_max_size = 50M/" /usr/local/etc/php/php.ini \
 && apk del autoconf g++ gcc make
# Install XDebug and copy development php.ini.
RUN if [ "$ENV" = "development" ]; then apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS \
 && pecl install xdebug-2.7.2 \
 && docker-php-ext-enable xdebug \
 && cp /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini \
 && echo "xdebug.remote_enable = On" >> /usr/local/etc/php/php.ini \
 && echo "xdebug.remote_host = host.docker.internal" >> /usr/local/etc/php/php.ini \
 && apk del .phpize-deps $PHPIZE_DEPS; \
    fi
# Copy files from the previous image.
COPY --from=composer --chown=www-data:www-data /app /app
# Expose port 9000.
EXPOSE 9000
# Start php-fpm server.
CMD ["php-fpm"]
